require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do

  context 'GET index' do

    it 'returns 200 response' do
      get :index
      expect(response).to be_success
    end

    it 'populates array of articles' do
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'b', body: 'bb')
      get :index
      expect(assigns[:articles].size).to eq 2
    end

    it 'gives the top 10 articles only' do
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      Article.create(title: 'a', body: 'aa')
      get :index
      expect(assigns[:articles].size).to eq 10
    end
  end

  context 'GET show' do
    let(:a) {Article.create(title: 'a', body: 'aa')}

    it 'returns 200 response' do
      get :show, params: { id: a.id }
      expect(response).to be_success
    end

    it 'renders the text corresponding to an article' do
      get :show, params: { id: a.id }
      expect(response).to render_template :show
    end
  end

  context 'DELETE destroy' do
    let(:a) {Article.create(title: 'a', body: 'aa')}

    it 'deletes the article' do
      expect(Article.where(id: a.id).count).to eq(1)

      delete :destroy, params: {id: a.id}
      expect(Article.where(id: a.id).count).to eq(0)
    end

    it 'redirects to the articles page' do
      delete :destroy, params: {id: a.id}
      assert_redirected_to articles_path
    end

    it 'deletes the comments associated with the article' do
      comment = a.comments.create(body: "something")
      expect(Comment.where(id: comment.id).count).to eq(1)

      delete :destroy, params: {id: a.id}
      expect(Comment.where(id: comment.id).count).to eq(0)
    end
  end
end
