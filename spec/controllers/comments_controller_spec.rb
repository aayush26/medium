require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  let(:article1) { Article.create(title: "aaa", body: "sssss") }
  let(:comment1) { article1.comments.create(body: "what is this") }

  context 'POST create' do
    it 'redirects to the article page after comment creation' do
      post :create, params: {article_id: article1.id, comment: comment1.attributes}
      assert_redirected_to article_path(article1.id)
    end
  end

  context 'DELETE action' do
    it 'deletes the comment correspoding to the article' do
      comment_id = comment1.attributes['id']
      delete :destroy, params: {article_id: article1.id, id: comment_id}
      expect(Comment.where(id: comment_id).count).to eq(0)
    end

    it 'redirects to the article page after comment deletion' do
      comment_id = comment1.attributes['id']
      delete :destroy, params: {article_id: article1.id, id: comment_id}
      assert_redirected_to article_path(article1.id)
    end
  end

  context 'UPDATE comment' do
    let(:comment_id) { comment1.attributes['id'] }

    it 'takes to edit page ' do
      get :edit, params: {article_id: article1.id, id: comment_id}
      expect(response.status).to eq(200)
    end

    it 'updates the new value of the comment' do
      comment_two = article1.comments.create(body: "changed")
      put :update, params: {article_id: article1.id, id: comment_id, comment: comment_two.attributes}
      expect(article1.comments[1][:body]).to eq("changed")
    end

    it 'redirects to the article page after comment updation' do
      comment_two = article1.comments.create(body: "changed")
      put :update, params: {article_id: article1.id, id: comment_id, comment: comment_two.attributes}
      assert_redirected_to article_path(article1.id)
    end
  end
end
