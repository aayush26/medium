require 'rails_helper'

RSpec.describe Article, type: :model do
  
  context 'Article validations' do
    it 'should return false if no title or body' do
      article = Article.new(title: "random")
      expect(article.valid?).to be_falsey

      article.title = ""
      article.body = "random"
      expect(article.valid?).to be_falsey
    end

    it 'should return true if title and body present' do
      article = Article.new(title: "random", body: "randomrandom")
      expect(article.valid?).to be_truthy
    end
  end

end
